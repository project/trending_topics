<?php

namespace Drupal\trending_topics\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Renderer;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * TrendingTopicsService Services for difference Tracker module.
 *
 * @ingroup trending_topics
 */
class TrendingTopicsService {
  use StringTranslationTrait;

  /**
   * A database connection service instance.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * A language manager service instance.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * Entity Manager for calss.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * TrendingTopicsService constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The Language service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   The entity type manager service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   */
  public function __construct(
    Connection $connection,
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entityManager,
    Renderer $renderer,
  ) {
    $this->database = $connection;
    $this->languageManager = $language_manager;
    $this->entityManager = $entityManager;
    $this->renderer = $renderer;
  }

  /**
   * Return trending topics.
   */
  public function getTrendingTopics(&$nodeTypes = NULL, &$itemNumber = NULL) {
    if (empty($itemNumber)) {
      $itemNumber = '5';
    }
    $current_language = $this->languageManager->getCurrentLanguage()->getId();
    $query = $this->database->select('node_field_data', 'nfd');
    $query->leftJoin('node_counter', 'nc', 'nfd.nid = nc.nid');
    $query->fields('nfd', ['title', 'nid'])
      ->condition('nfd.status', '1')
      ->orderBy('nc.totalcount', 'DESC')
      ->range(0, $itemNumber);
    if (!empty($current_language) && strtolower($current_language) != 'und') {
      $query->condition('nfd.langcode', $current_language);
    }
    if (!empty($nodeTypes)) {
      $query->condition('nfd.type', $nodeTypes, 'IN');
    }
    $results = $query->execute()->fetchAll();
    $trending_topics = '<div id="trending-topics">';
    foreach ($results as $result) {
      $url = Url::fromRoute('entity.node.canonical', ['node' => $result->nid]);
      $topic_link = Link::fromTextAndUrl($result->title, $url);
      $topic_link = $topic_link->toRenderable();
      $trending_topics .= '<div class="trending-topic-list">' . $this->renderer->render($topic_link) . '</div>';
    }
    $trending_topics .= '</div>';
    return $trending_topics;
  }

}
