<?php

namespace Drupal\trending_topics\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Trending Topics List' block.
 *
 * @Block(
 *   id = "trending_topics_list_block",
 *   admin_label = @Translation("Trending Topics"),
 * )
 */
class TrendingTopicsListBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The trending topics.
   *
   * @var \Drupal\trending_topics\Services\TrendingTopicsService
   */
  protected $trendingTopics;

  /**
   * The entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs an EventCountdownBlock object.
   *
   * @param array $configuration
   *   The block configuration.
   * @param string $plugin_id
   *   The ID of the plugin.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param mixed $entity_type_manager
   *   The entity type manager.
   * @param mixed $trendingTopics
   *   The trending topics.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $entity_type_manager,
    $trendingTopics,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->trendingTopics = $trendingTopics;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('trending_topics')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $nodeTypes = $itemNumber = NULL;
    if (!empty($config['node_type'])) {
      $nodeTypes = $config['node_type'];
    }
    if (!empty($config['item_number'])) {
      if ($config['item_number'] != 'other') {
        $itemNumber = $config['item_number'];
      }
      else {
        $itemNumber = $config['item_number_other'];
      }
    }
    $trending_topics = $this->trendingTopics->getTrendingTopics($nodeTypes, $itemNumber);
    return [
      '#type' => 'markup',
      '#markup' => $trending_topics,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $nodeTypes = [];
    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($node_types as $type) {
      $nodeTypes[$type->getOriginalId()] = $type->label();
    }
    $form['node_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content Types'),
      '#description' => $this->t('Content Type you want to include in list. If not selected then all content types will be included.'),
      '#options' => $nodeTypes,
      '#multiple' => TRUE,
      '#default_value' => $config['node_type'] ?? '',
    ];
    $form['item_number'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of Item in List?'),
      '#description' => $this->t('Number of item you want to show.'),
      '#options' => [
        '3' => '3',
        '5' => '5',
        '10' => '10',
        '15' => '15',
        'other' => $this->t('other'),
      ],
      '#default_value' => $config['item_number'] ?? '',
    ];
    $form['item_number_other'] = [
      '#type' => 'number',
      '#default_value' => $config['item_number_other'] ?? '',
      '#states' => [
        'visible' => [
          ':input[name="settings[item_number]"]' => ['value' => 'other'],
        ],
        'required' => [
          ':input[name="settings[item_number]"]' => ['value' => 'other'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['node_type'] = $values['node_type'];
    $this->configuration['item_number'] = $values['item_number'];
    $this->configuration['item_number_other'] = $values['item_number_other'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    if (($form_state->getValue('item_number') == 'other')) {
      if (empty($form_state->getValue('item_number_other'))) {
        $form_state->setErrorByName('item_number_other', $this->t('Please input number of items.'));
      }
      elseif (!is_numeric($form_state->getValue('item_number_other'))) {
        $form_state->setErrorByName('item_number_other', $this->t('Only numbers allowed.'));
      }
    }
  }

}
