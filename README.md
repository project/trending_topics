# Trending Topics

The Trending Topics module helps to list the most visited content.

- For a full description of the module visit
  [project page](https://www.drupal.org/project/trending_topics).

- To submit bug reports and feature suggestions, or to track changes visit
  [issue queue](https://www.drupal.org/project/issues/trending_topics).


## Contents of this file

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.
- This module require Statistic module, to get content view counts.
- Go to `Administration >> Configuration >> System >> Statistics`
- Enable Count content views.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/
extending-drupal/installing-drupal-modules) for further information.


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


## Maintainers

- Pravin Gaikwad - [Rajeshreeputra](https://www.drupal.org/u/rajeshreeputra)
